﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounter
{
    public class Class1
    {
        public int total;

        public Compteur()
        {
            total = 0;
        }

        public string incrementation()
        {
            total = total + 1;
            return string.Format("{0}", total);
        }

        public string decrementation()
        {
            total = total - 1;
            return string.Format("{0}", total);
        }

        public string raz()
        {
            total = 0;
            return string.Format("{0}", total);
        }

    }
}
